package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {
}
