package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    boolean existsById(String id);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findById(String id);

    M findByIndex(Integer index);

    int getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}
