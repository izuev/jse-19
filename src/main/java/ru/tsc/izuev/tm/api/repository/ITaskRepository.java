package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
