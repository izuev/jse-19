package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isNotAuth();

    String getUserId();

    User getUser();

}
