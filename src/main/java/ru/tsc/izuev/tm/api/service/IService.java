package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.api.repository.IRepository;
import ru.tsc.izuev.tm.enumerated.Sort;
import ru.tsc.izuev.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
