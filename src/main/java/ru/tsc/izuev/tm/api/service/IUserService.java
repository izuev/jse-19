package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.api.repository.IUserRepository;
import ru.tsc.izuev.tm.enumerated.Role;
import ru.tsc.izuev.tm.model.User;

public interface IUserService extends IService<User>, IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String middleName, String lastName);

}
