package ru.tsc.izuev.tm.command;

import ru.tsc.izuev.tm.api.model.ICommand;
import ru.tsc.izuev.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = name != null && !name.isEmpty();
        if (hasName) result += name;
        if (argument != null && !argument.isEmpty()) result += hasName ? ", " + argument : argument;
        if (description != null && !description.isEmpty()) result += " : " + description;
        return result;
    }

}
