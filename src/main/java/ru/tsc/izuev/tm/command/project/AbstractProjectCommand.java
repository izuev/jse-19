package ru.tsc.izuev.tm.command.project;

import ru.tsc.izuev.tm.api.service.IProjectService;
import ru.tsc.izuev.tm.api.service.IProjectTaskService;
import ru.tsc.izuev.tm.command.AbstractCommand;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public abstract class AbstractProjectCommand extends AbstractCommand {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DATE_FORMAT.format(project.getCreated()));
    }

    @Override
    public String getArgument() {
        return null;
    }

}
