package ru.tsc.izuev.tm.command.project;

import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-start-by-index";

    private static final String DESCRIPTION = "Start project by index.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
