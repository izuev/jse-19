package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.api.service.IProjectTaskService;
import ru.tsc.izuev.tm.api.service.ITaskService;
import ru.tsc.izuev.tm.command.AbstractCommand;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void renderTasks(final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DATE_FORMAT.format(task.getCreated()));
    }

    @Override
    public String getArgument() {
        return null;
    }

}
