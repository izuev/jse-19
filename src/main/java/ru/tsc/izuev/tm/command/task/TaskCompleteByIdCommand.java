package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-complete-by-id";

    private static final String DESCRIPTION = "Complete task by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
