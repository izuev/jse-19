package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private static final String NAME = "task-create";

    private static final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
