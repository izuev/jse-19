package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.model.Task;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-id";

    private static final String DESCRIPTION = "Show task by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findById(id);
        showTask(task);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
