package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.model.Task;
import ru.tsc.izuev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-project-id";

    private static final String DESCRIPTION = "Show tasks by project id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(id);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
