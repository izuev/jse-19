package ru.tsc.izuev.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "user-logout";

    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
