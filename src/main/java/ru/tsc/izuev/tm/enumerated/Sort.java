package ru.tsc.izuev.tm.enumerated;

import ru.tsc.izuev.tm.comparator.CreatedComparator;
import ru.tsc.izuev.tm.comparator.NameComparator;
import ru.tsc.izuev.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator<?> comparator;

    Sort(final String displayName, final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String displayName) {
        if (displayName == null || displayName.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.displayName.equals(displayName)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
