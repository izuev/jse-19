package ru.tsc.izuev.tm.exception.system;

public final class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}
