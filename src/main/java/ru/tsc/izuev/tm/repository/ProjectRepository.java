package ru.tsc.izuev.tm.repository;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
}
