package ru.tsc.izuev.tm.repository;

import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task : models) {
            if (projectId.equals(task.getProjectId())) tasksByProjectId.add(task);
        }
        return tasksByProjectId;
    }

}
