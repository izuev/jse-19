package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.repository.IRepository;
import ru.tsc.izuev.tm.api.service.IService;
import ru.tsc.izuev.tm.enumerated.Sort;
import ru.tsc.izuev.tm.exception.field.IdEmptyException;
import ru.tsc.izuev.tm.exception.field.IndexIncorrectException;
import ru.tsc.izuev.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(final String id) {
        return repository.existsById(id);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<M> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

}
