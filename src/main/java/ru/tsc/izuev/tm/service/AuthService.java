package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.service.IAuthService;
import ru.tsc.izuev.tm.api.service.IUserService;
import ru.tsc.izuev.tm.exception.field.LoginEmptyException;
import ru.tsc.izuev.tm.exception.field.PasswordEmptyException;
import ru.tsc.izuev.tm.exception.system.AccessDeniedException;
import ru.tsc.izuev.tm.model.User;
import ru.tsc.izuev.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isNotAuth() {
        return userId == null;
    }

    @Override
    public String getUserId() {
        if (isNotAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (isNotAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}
